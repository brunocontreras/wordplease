from django.contrib import admin
from models import Post, Category

class PostAdmin(admin.ModelAdmin):

    list_display = ('title', 'imageUrl', 'summary', 'publish_on')
    list_filter = ('publish_on', 'categories')
    search_fields = ('title', 'summary', 'content')

    fieldsets = (
        ('Title', {
            'classes': ('wide',),
            'fields': ('title',)
        }),
        ('Image', {
            'classes': ('wide',),
            'fields': ('imageUrl',)
        }),
        ('Content', {
            'classes': ('wide',),
            'fields': ('summary', 'content',)
        }),
        ('Data', {
            'classes': ('wide',),
            'fields': ('owner', 'publish_on', 'categories',)
        })
    )

admin.site.register(Post, PostAdmin)
admin.site.register(Category)