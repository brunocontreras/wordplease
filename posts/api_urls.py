from django.conf.urls import url, include
from posts.api import UserViewSet, BlogListAPIView, PostViewSet
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'posts/(?P<username>[\w-]+)', PostViewSet)

urlpatterns = [
    url(r'', include(router.urls)),
    url(r'blogs/', BlogListAPIView.as_view()),
]
