from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone
from posts.managers import PostManager


class Category(models.Model):

    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name



class Post(models.Model):

    objects = PostManager()

    owner = models.ForeignKey(User, related_name='post')
    title = models.CharField(max_length=256)
    imageUrl = models.URLField(null=True, blank=True)
    summary = models.TextField()
    content = models.TextField()
    publish_on = models.DateTimeField(default=timezone.now)
    categories = models.ManyToManyField(Category, blank=True)

    def __unicode__(self):
        return self.title + " (" + self.owner.username + ")"

    class Meta:
        ordering = ['-publish_on']