# -*- coding: utf-8 -*-
from posts.models import Post
from rest_framework.permissions import BasePermission

class UserPermission(BasePermission):

    def has_permission(self, request, view):
        if request.user.is_superuser:
            return True
        elif view.action:
            if view.action.lower() == 'create' and not request.user.is_authenticated():
                return True
            elif view.action.lower() in ['retrieve', 'update', 'destroy', 'partial_update']:
                return True
        return False


    def has_object_permission(self, request, view, obj):
        return obj == request.user or request.user.is_superuser


class PostPermission(BasePermission):

    def has_permission(self, request, view):
        if request.user.is_superuser:
            return True
        elif view.action:
            if view.action.lower() == 'create' and not request.user.is_authenticated():
                return True
            elif view.action.lower() in ['list', 'retrieve', 'update', 'destroy', 'partial_update']:
                return True
        return False

    def has_object_permission(self, request, view, obj):
        if obj.owner == request.user or request.user.is_superuser:
            return True
        elif view.action and view.action.lower() == 'retrieve':
            return obj in Post.objects.published_posts()
        return False
