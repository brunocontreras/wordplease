from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from posts.models import Post
from django.contrib.auth.decorators import login_required

class PostGetQuerySet(object):

    def get_queryset(self):
        username = self.kwargs.get('username')
        owner = get_object_or_404(User, username=username)
        return Post.objects.published_posts().filter(owner=owner)


class BlogGetQuerySet(object):

    def get_queryset(self):
        return User.objects.filter(post__in=Post.objects.published_posts()).distinct()


class LoginRequiredMixin(object):
    @classmethod
    def as_view(cls, **initkwargs):
        view = super(LoginRequiredMixin, cls).as_view(**initkwargs)
        return login_required(view)