from django.contrib.auth.models import User
from posts.models import Post
from rest_framework import serializers
from rest_framework.reverse import reverse


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'username', 'email', 'password']
        read_only_fields = ['id']


class BlogSerializer(serializers.HyperlinkedModelSerializer):

    url = serializers.HyperlinkedIdentityField(view_name='post-list', lookup_field='username')

    class Meta:
        model = User
        fields = ['username', 'url']


class PostSerializer(serializers.ModelSerializer):
    owner = UserSerializer()
    categories = serializers.StringRelatedField(many=True)

    class Meta:
        model = Post
        read_only_fields = ['owner']

class PostListSerializer(PostSerializer):
    class Meta(PostSerializer.Meta):
        fields = ['title', 'imageUrl', 'summary', 'publish_on']