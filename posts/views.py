from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import redirect
from django.views.generic import ListView, DetailView, CreateView, FormView
from models import Post
from posts.forms import SignUpForm
from posts.mixins import PostGetQuerySet, LoginRequiredMixin, BlogGetQuerySet


class HomeView(ListView):
    template_name = 'posts/post_list.html'
    queryset = Post.objects.published_posts


class BlogListView(BlogGetQuerySet, ListView):
    template_name = 'posts/blog_list.html'


class UserPostsListView(PostGetQuerySet, ListView):
    template_name = 'posts/post_list.html'


class PostDetailView(PostGetQuerySet, DetailView):
    template_name = 'posts/post_detail.html'


class PostCreateView(LoginRequiredMixin, CreateView):
    model = Post
    fields = ['title', 'imageUrl', 'summary', 'content', 'publish_on', 'categories']

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super(PostCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('post_detail', kwargs={'username': self.request.user.username, 'pk': self.object.pk})


class SignupView(FormView):
    template_name = 'registration/signup.html'
    form_class = SignUpForm

    def form_valid(self, form):
        form.save()
        username = form.cleaned_data.get('username', '')
        password = form.cleaned_data.get('password1', '')
        user = authenticate(username=username, password=password)
        login(self.request, user)
        return redirect('home')
