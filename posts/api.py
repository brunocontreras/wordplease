from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from posts.mixins import BlogGetQuerySet
from posts.models import Post
from posts.permissions import UserPermission, PostPermission
from posts.serializers import UserSerializer, BlogSerializer, PostListSerializer, PostSerializer
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework import viewsets, mixins
from rest_framework.generics import ListAPIView, get_object_or_404
from rest_framework.viewsets import ModelViewSet


def update_password(serializer):
    password = make_password(serializer.validated_data.get('password'))
    serializer.save(password=password)


class UserViewSet(mixins.CreateModelMixin,
                  mixins.RetrieveModelMixin,
                  mixins.DestroyModelMixin,
                  mixins.UpdateModelMixin,
                  viewsets.GenericViewSet):

    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [UserPermission]

    def perform_create(self, serializer):
        update_password(serializer)

    def perform_update(self, serializer):
        update_password(serializer)


class BlogListAPIView(BlogGetQuerySet, ListAPIView):
    serializer_class = BlogSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    search_fields = ['username']
    ordering_fields = ['username']


class PostViewSet(ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = [PostPermission]
    filter_backends = [SearchFilter, OrderingFilter]
    search_fields = ['title', 'content']
    ordering_fields = ['title', 'publish_on']

    def get_queryset(self):
        username = self.kwargs.get('username')
        owner = get_object_or_404(User, username=username)
        if not self.request.user.is_authenticated() or self.request.user != owner:
            return Post.objects.published_posts().filter(owner=owner)
        elif self.request.user.is_superuser:
            return Post.objects.filter(owner=owner)

    def get_serializer_class(self):
        if self.action and self.action.lower() == 'list':
            return PostListSerializer
        return PostSerializer

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)