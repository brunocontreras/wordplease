from django.contrib.auth.models import User
from django.db.models.query import QuerySet
from django.utils import timezone

class PostQuerySet(QuerySet):

    def published_posts(self):
        return self.filter(publish_on__lte=timezone.now())

PostManager = PostQuerySet.as_manager