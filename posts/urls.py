from django.conf.urls import url
from django.contrib.auth import views as auth_views
from posts import views


urlpatterns = [
    url(r'^$', views.HomeView.as_view(), name='home'),
    url(r'^blogs/$', views.BlogListView.as_view(), name='blog_list'),
    url(r'^blogs/(?P<username>[\w-]+)$', views.UserPostsListView.as_view(), name='user_posts_list'),
    url(r'^blogs/(?P<username>[\w-]+)/(?P<pk>[0-9]+)$', views.PostDetailView.as_view(), name='post_detail'),
    url(r'^new-post$', views.PostCreateView.as_view(), name='post_create'),

    # Users
    url(r'^login$', auth_views.login, name='login'),
    url(r'^logout$', auth_views.logout, { 'next_page': 'home' }, name='logout'),
    url(r'^signup$', views.SignupView.as_view(), name='signup'),
]
